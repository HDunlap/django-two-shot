from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from .forms import CreateReceiptForm, CreateCategoryForm, CreateAccountForm


# Create your views here.

@login_required(login_url='login')
def receipt_list(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipt,
    }
    return render(request, "receipts/list.html", context)


@login_required(login_url='login')
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = CreateReceiptForm
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required(login_url='login')
def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category": category
    }
    return render(request, "categories/list.html", context)


@login_required(login_url='login')
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        "account": account
    }
    return render(request, "accounts/list.html", context)


@login_required(login_url='login')
def create_category(request):
    if request.method == "POST":
        form = CreateCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CreateCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


@login_required(login_url='login')
def create_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = CreateAccountForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)
