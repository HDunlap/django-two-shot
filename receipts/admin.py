from django.contrib import admin
from .models import ExpenseCategory, Account, Receipt


class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "owner",
    ]


admin.site.register(ExpenseCategory, ExpenseCategoryAdmin)


class AccountAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "number",
        "owner",
    ]


admin.site.register(Account, AccountAdmin)


class ReceiptAdmin(admin.ModelAdmin):
    list_display = [
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
    ]


admin.site.register(Receipt, ReceiptAdmin)
