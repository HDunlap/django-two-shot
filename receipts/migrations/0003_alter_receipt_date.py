# Generated by Django 4.1.5 on 2023-01-20 00:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0002_receipt_account_receipt_category_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="date",
            field=models.DateTimeField(),
        ),
    ]
